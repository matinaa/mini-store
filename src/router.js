import Vue from 'vue'
import Router from 'vue-router'
import CheckOut from "./components/CheckOut"
import ProductList from "./components/ProductList"
import ShoppingCart from "./components/ShoppingCart"
import Product from "./components/Product"


Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/check',
            component: ShoppingCart

        },
        {
            path: '/',
            component: ProductList

        },
        {
            path: '/product/:id',
            name: 'product',
            props:true,
            component: Product
        }

    ]
})