import Vue from 'vue'
// import Veutify from 'vuetify'
import App from './App.vue'
import 'vuetify/dist/vuetify.min.css'
// import VueRouter from 'vue-router'
import router from './router'

import store  from "./store";
var $ = require('jquery');
window.$ = $;

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
// import {currency} from  "./currency"


// Vue.use(Veutify);
// Vue.use(VueRouter);
//
//
//
// const router = new VueRouter({ router, mode: 'history'})


new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')

Vue.config.productionTip = false