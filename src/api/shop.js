/**
 * Mocking client-server processing
 */
const products = [
  {"id": 1, "title": "iPad 4 Mini", "price": 500.01, "size":"small",image:"https://images-na.ssl-images-amazon.com/images/I/71qP%2BfMtbDL._SL1500_.jpg"},
  {"id": 2, "title": "H&M T-Shirt White", "price": 10.99, "size":"med",image:"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRAxXS75kXehr3JshGoDK9blcuz8reOuHWNxtbxcQ_knpl-uPPw&usqp=CAU"},
  {"id": 3, "title": "Charli XCX - Sucker CD", "price": 19.99,"size":"small",image:"https://www.marni.com/12/12386489MT_13_n_r.jpg"},
  {"id": 4, "title": "iPad 4 Mini", "price": 500.01,"size":"small",image:"https://static.pullandbear.net/2/photos/2020/V/0/1/p/5234/222/250/5234222250_1_1_3.jpg?t=1582708649855"},
  {"id": 5, "title": "H&M T-Shirt White", "price": 10.99, "size":"med",image:"https://www.asmc.com/media/image/ea/5a/45/15011-0.jpg"},
  {"id": 6, "title": "Charli XCX - Sucker CD", "price": 19.99, "size":"small",image:"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT-HcBYiclSpx8ZpgVGcisABzWlz84vlXTS5SznqjUCC-FIsU-j&usqp=CAU"}
]

export default {
  getProducts (cb) {
    setTimeout(() => cb(products), 100)
  },

  buyProducts (products, cb, errorCb) {
    setTimeout(() => {
      // simulate random checkout failure.
      (Math.random() > 0.5 || navigator.webdriver)
        ? cb()
        : errorCb()
    }, 100)
  }
}
