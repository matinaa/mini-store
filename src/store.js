import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import axios from "axios";
import shop from "@/api/shop";


const URL = 'https://fakestoreapi.com/products';

let cart=window.localStorage.getItem('cart');

export default new Vuex.Store ({
    state: {
        products:  [],
        // cartItem:[],
        cart: cart ? JSON.parse(cart): [],
        // product: {},

        // loading: true
        // currentVehicle: {},

    },
    actions: {

        loadData({commit}) {

            return new Promise((resolve, reject) => {


                shop.getProducts(products => {
                    // this.products =products
                    commit('getProduct', products)
                    resolve()

                // axios.get(URL).then((response) => {
                //     commit('getProduct', response.data)
                //     resolve()
                })
            })

        },


        loadProduct({commit}, id) {
            axios.get(`https://fakestoreapi.com/products/${id}`).then((response) => {
                commit('getProduct', response.data)
            })
        },


        addProductToCart({commit,state}, product) {


            for (let item of state.cart) {
                console.log(99, item)
                if (item.id == product.id )
                {
                  commit('incrementItemQuantity', product.id)
                    commit('saveData')

                    return;
                }

            }


         commit('pushProductToCart', product)
            commit('saveData')

        },



        removeItem({commit}, pid) {
           commit("REMOVE_Item", pid);
            commit('saveData')
        },



    },


    mutations: {


        saveData(state ){
            window.localStorage.setItem('cart', JSON.stringify(state.cart))
        },


        getProduct(state, product) {
            state.products = product
        },


        //
        // ADD_Item(state, productId) {
        //     state.cart.push({
        //         id: productId,
        //         quantity: 1
        //     });
        // },




        REMOVE_Item (state, pid  ) {

            for(let product of state.cart)
            {

                    if(product.id == pid)

                        if( product.quantity>1)
                        {
                        product.quantity --
                            return;
                         }

            }

                state.cart.splice(state.cart.indexOf(pid) ,1);

                  // state.cart.splice(pid, 1);

        },





        //
        // setProducts(state, products){
        //     state.products=products
        // },






        pushProductToCart (state, product) {

            state.cart.push({
                product: product,
                id:product.id,
                quantity: 1
            })

        },
        //
        incrementItemQuantity (state, pid) {

            for(let product of state.cart)
            {
                if(product.id == pid)

                // if(product.product.inventory !== 0 )

                        product.quantity ++
            }
        },




        emptyCart (state) {
            state.cart = []
        }
    },

    getters: {


            currentProduct: state => state.products,

            avaibleProducts(state, getters) {

                return state.products.filter(product => product)
            },






        // cartProducts(state , product) {
        //     return state.cart.map(cartItem => {
        //        let product = state.products.find(product=> product.id == cartItem.id)
        //
        //
        //         return {
        //             title: product.title,
        //             price: product.price,
        //             image:product.image,
        //             quantity: cartItem.quantity,
        //
        //         }
        //     })
        // },



        // cartTotal(state, getters) {
        //
        //     return getters.cartProduct.reduce((total, product) => total + product.price * product.quantity, 0)
        // }


        cartTotal(state) {
                let total=0;

                state.cart.forEach(item=>{
                    total += item.product.price *item.quantity
                })

            return total

        }



        },
})

